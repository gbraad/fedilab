Fedilab é un cliente multifuncional Android para acceder ao Fediverso distribuído, consistente en micro publicación, compartición de imaxes e hospedaxe de vídeo.

Soporta:
- Mastodon, Pleroma, Peertube, GNU Social, Friendica.


The app has advanced features (especially for Pleroma and Mastodon):

- Soporte multi-conta
- Mensaxes programadas desde o dispositivo
- Promocións programadas
- Mensaxes marcadas
- Seguimento e interacción con instancias remotas
- Acalar contas de xeito temporal
- Cross-account actions with a long press
- Tradución incluída
- Liña temporal Artística
- Liña temporal de Vídeos
