Added
- Twitter URLs can be replaced with Nitter (or one of its instances)
- Take notes for accounts
- Delayed live notifications
- Store in drafts for quick replies

Changed
- Improve Invidious support (local=true, more URLs detected)
- Set the type of live notifications (live, delayed, none).
- Improve auto-split feature

Fixed
- Unknown URL scheme with built-in browser
- Fix mentions
- Crash when clicking on thumbs-up button for Peertube videos with a Mastodon account
- Fix a scrolling issue
- Fix Peertube playlist 1.4+